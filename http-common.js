import axios from 'axios';
const token = localStorage.token;
export const HTTP = axios.create({
	baseURL: 'http://api.who.test.turbovizgps.com/api/',
	headers: {
		Authorization: `Bearer ${token}`
	}
})