export default {
    requesterName: function(newValue, oldValue) {
        this.pickupName = newValue;
        console.log('requesterName => ',newValue);
    },
    pickupName: function(newValue, oldValue) {
        console.log('pickupname => ',newValue);
        if(newValue !== '') {
            let searchAddress = [];
            let searchTerm = newValue.toLowerCase();
            this.addressesArray.forEach(address => {

                if(address.name.toLowerCase().includes(searchTerm)) {
                    searchAddress.push(address);
                }
            });
            this.pickupAddressList = searchAddress;
        } 
        else {
            this.pickupAddressList = this.addressesArray;
        }
        
    },
    deliveryName: function(newValue, oldValue) {
        console.log('deliveryname => ',newValue);
        if(newValue !== '') {
            let searchAddress = [];
            let searchTerm = newValue.toLowerCase();
            this.addressesArray.forEach(address => {

                if(address.name.toLowerCase().includes(searchTerm)) {
                    searchAddress.push(address);
                }
            });
            this.deliveryAddressList = searchAddress;
        } 
        else {
            this.deliveryAddressList = this.addressesArray;
        }
    },
    selectedDeliveryServices(newValue, oldValue) {
        console.log('Hello:',newValue);
        this.deliveryServices = [];
        this.currentProvider = newValue;
        this.currentProvider.services.forEach(item => {
                this.deliveryServices.push(item);
        });
        console.log('List',this.deliveryServices);
    },
    toLower(str){
        return (str == null) ? '' : str.toLowerCase();
    },
    itemType: function(newValue, oldValue) {
        this.currentGoods = this.goodsList.find(item => item.type == newValue);
        if(oldValue != '') {
            this.changeGood = true; 
        }
        if(localStorage.currentGoods && newValue != JSON.parse(localStorage.currentGoods).type) {
            console.log('goods changed');    
        }
        else {
            this.currentGoods = this.goodsList.find(item => item.type == newValue); 
            if(!this.currentGoods) {
                this.currentGoods = this.goodsList[0];
            }
        }
        this.goodsDispatchDetails = this.currentGoods.modes;
    },
    modeOfDispatch: function(newValue, oldValue) {
        this.addProvider(newValue);  
    },
    service: function(newValue, oldValue) {
        let serviceId = newValue.split('@')[1];
        console.log(JSON.stringify(this.currentProvider));
        let selectedservice = this.currentProvider.services.find(data => data.id === serviceId);
        this.selectedServiceType = selectedservice.type;
        localStorage.selectedServiceType = selectedservice.type;
        this.providerLogo = this.currentProvider.logo; 
        this.providerDescription = this.currentProvider.description;
        this.providerRatecard = this.currentProvider.rate_card;      
    },
    selectedDeliveryServices(newValue, oldValue) {
        this.deliveryServices = [];
        this.currentProvider = newValue;
        this.currentProvider.services.forEach(item => {
                this.deliveryServices.push(item);
        });
        console.log('List',this.deliveryServices);
        if(this.deliveryServices.length == 1){
            this.service = this.deliveryServices[0].name + '@' + this.deliveryServices[0].id;
        }else{
            this.service = "";
        }
    },
}
