import router from '../../../router';
export default {
    /**
     * Fill the courier details
     * 
     * @param {[uuid]} id [uuid of the courier]
     */
    setDraft(draft) {
        // let couriers = JSON.parse(localStorage.courierDetails);
        let goods = JSON.parse(localStorage.goods);
        let currentCourier = draft;
        if(currentCourier.type_of_good == localStorage.currentEditGood) {
            this.currentGoods = goods.find(item => item.type == currentCourier.type_of_good);
            
            this.fillData(currentCourier);
            this.fillOldDetails(currentCourier);
        } else {
            this.currentGoods = goods.find(item => item.type == localStorage.currentEditGood);
            this.fillDetails();
        }

        if(this.currentGoods.volume == 'N') {
            this.isDisabled = true;
        }
        else if(this.currentGoods.volume == 'Y') {
            this.isDisabled = false;
        }
        
    },
    /**
     * Fetch the draft details
     * 
     */
    fetchDraftDetails() {
        this.axios.get(`draft/${this.$route.params.id}`)
        .then(response => {
            console.log('response draft',response);
            this.draftDetails = response.data.draft;
            this.setDraft(response.data.draft);
        })
        .catch(error => {
            if(error) {
                console.log('error draft',error.response);
            }
        })
    },
    fillOldDetails(currentCourier) {
        console.log('currentCourier on older details:', currentCourier);
        this.currentWeight = currentCourier.item_weight ? currentCourier.item_weight : '0';
        this.currentWidth = currentCourier.item_width ? currentCourier.item_width : '0';
        this.currentHeight = currentCourier.item_height ? currentCourier.item_height : '0';
        this.currentLength = currentCourier.item_length ? currentCourier.item_length : '0';
        this.currentm3 = currentCourier.item_m3 ? currentCourier.item_m3 : '0';
        this.currentCubicKg = currentCourier.item_cubic_kg ? currentCourier.item_cubic_kg : '0';
        this.currentChargeUnit = currentCourier.item_charge_unit ? currentCourier.item_charge_unit : '0'

    },
    fillDetails() {
        this.currentWeight = this.currentGoods.weight ? this.currentGoods.weight : '0';
        this.currentWidth = this.currentGoods.width ? this.currentGoods.width : '0';
        this.currentHeight = this.currentGoods.height ? this.currentGoods.height : '0';
        this.currentLength = this.currentGoods.length ? this.currentGoods.length : '0';
        this.currentm3 = this.currentGoods.m3 ? this.currentGoods.m3 : '0';
        this.currentCubicKg = this.currentGoods.cubic_kg ? this.currentGoods.cubic_kg : '0';
        this.currentChargeUnit = this.currentGoods.charge_unit ? this.currentGoods.charge_unit : '0'

    },
    fillData (currentCourier) {
        console.log('currentCourier on fill data:', currentCourier);
        //let currentCourier = JSON.parse(localStorage.currentCourier);
        this.itemName = currentCourier.item_reference;
        this.itemCount = currentCourier.item_qty || '1' ;
        this.goodsDescription = currentCourier.goods_description;
        this.remarks = currentCourier.remarks;
        this.packageLocation = currentCourier.package_location;
        this.pickupInstructions = currentCourier.pickup_instructions;
        this.pickupDate = currentCourier.pickup_date;
        this.pickupTime = currentCourier.pickup_time || '08:00';
        this.officeCloseTime = currentCourier.office_close_time || '18:00';         
    },
     storeData() {
        console.log('currentGoods:', this.currentGoods);
        this.courierBookingData.item_reference = this.itemName;
        this.courierBookingData.item_weight = this.currentWeight? this.currentWeight : '0' ;
        this.courierBookingData.item_qty = this.itemCount;
        this.courierBookingData.item_length = this.currentLength;
        this.courierBookingData.item_width = this.currentWidth;
        this.courierBookingData.item_height = this.currentHeight;
        this.courierBookingData.item_m3 = this.currentm3;
        this.courierBookingData.item_cubic_kg = this.currentCubicKg;
        this.courierBookingData.item_charge_unit = this.currentChargeUnit;
        this.courierBookingData.item_is_dg = this.currentGoods.is_dg ? '1' : '0';
        this.courierBookingData.item_is_food = this.currentGoods.is_food ? '1' : '0';
        this.courierBookingData.goods_description = this.goodsDescription;
        this.courierBookingData.remarks = this.remarks;
        this.courierBookingData.package_location = this.packageLocation;
        this.courierBookingData.pickup_instructions = this.pickupInstructions;
        this.courierBookingData.pickup_date = this.pickupDate || new Date().toJSON().slice(0,10).replace(/-/g,'-');
        this.courierBookingData.pickup_time = this.pickupTime;
        this.courierBookingData.office_close_time = this.officeCloseTime;

        let itemDetails = {
            weight: this.currentWeight,
            width: this.currentWidth,
            height: this.currentHeight,
            length: this.currentLength,
            m3: this.currentm3,
            cubic_kg: this.currentCubicKg,
            charge_unit: this.currentChargeUnit
        }
        console.log('itemDetails:', itemDetails);
        let temp = JSON.parse(localStorage.currentGoods);

        let updatedItemDetails = Object.assign(temp, itemDetails);
        console.log('updated item details:', updatedItemDetails);
        localStorage.currentGoods = JSON.stringify(updatedItemDetails);

        let data = JSON.parse(localStorage.courierBookingData);
        let updatedData = Object.assign(data, this.courierBookingData);
        localStorage.courierBookingData = JSON.stringify(updatedData);
        console.log('c:', this.courierBookingData);
        console.log('data:', updatedData);

        //Draft functionality
        let draftData = {
            item_reference: this.courierBookingData.item_reference,
            item_weight: this.courierBookingData.item_weight,
            item_qty: this.courierBookingData.item_qty,
            item_length: this.courierBookingData.item_length,
            item_width: this.courierBookingData.item_width,
            item_height: this.courierBookingData.item_height,
            item_m3: this.courierBookingData.item_m3,
            item_cubic_kg: this.courierBookingData.item_cubic_kg,
            item_charge_unit: this.courierBookingData.item_charge_unit,
            item_is_dg: this.courierBookingData.item_is_dg,
            item_is_food: this.courierBookingData.item_is_food,
            goods_description: this.courierBookingData.goods_description,
            remarks: this.courierBookingData.remarks,
            declared_value: this.courierBookingData.declared_value,
            insured_value: this.courierBookingData.insured_value,
            package_location: this.courierBookingData.package_location,
            pickup_instructions: this.courierBookingData.pickup_instructions,
            pickup_date: this.courierBookingData.pickup_date,
            pickup_time: this.courierBookingData.pickup_time,
            office_close_time: this.courierBookingData.office_close_time
        }
        console.log('Draft:',draftData);
        this.axios.put(`draft/${this.$route.params.id}/second`, draftData)
        .then(response => {
            console.log(response);
            console.log('second api updated');
        })
        .catch(error => {
            console.log(error.response);
        })

        this.checkEmptyValue();
    },
     warningBox(key) {
        this.$refs[key].classList.add('warning');
    },
    removeWarning(item) {
        this.$refs[item].classList.remove('warning');
    },
    checkEmptyValue() {
        let flag = true;
       Object.keys(this.courierBookingData).forEach((key) => {
                    if(!this.courierBookingData[key] && key != 'item_reference' && key != 'item_width' && key != 'item_height' && key != 'item_m3' && key != 'item_cubic_kg' && key != 'item_charge_unit' && key != 'item_length' && key != 'goods_description' && key != 'remarks') {
                        console.log(key);
                        this.warningBox(key);
                        flag = false;
                    }
                });
        if(flag) {
            router.push({
                name: 'EditDraftConfirm',
                params: {
                    id: this.$route.params.id
                }
            });
        }
        else {
            this.$toasted.show("All fields are required", { 
                 theme: "primary", 
                 position: "bottom-right", 
                 type: 'error',
                 duration: 2000,
                 iconPack : 'material',
                 icon : {
                    name : 'close'
                 }
            });
        }
    },
    previousWindow() {
        router.push({
            name: 'EditDraftService',
            params: {
                id: this.$route.params.id
            }
        });
        location.reload();
    }
}
