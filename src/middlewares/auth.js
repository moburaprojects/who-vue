import Vue from 'vue';
import Toasted from 'vue-toasted';

Vue.use(Toasted)

const exclude = [
	'/',
	'/signup',
	'/verify',
	'/forgot/password',
	'/reset/password/*',
	'/already-verified'
];

const redirect = '/';

export default (to, from, next) => {
	if(!regInclude(exclude, to.path)) {
		if(getTimeDifference() > getExpiry()) {
			localStorage.clear();
			Vue.toasted.show('Your session expired. Please login to continue.', { 
                 theme: "primary", 
                 position: "bottom-right", 
                 duration: 2000,
                 iconPack : 'material',
                 icon : {
                    name : 'close'
                 }
            });
            return next({ path: '/' });
		}
	}
	return next();
}

let getTimeDifference = () => {
	return (localStorage.timeStamp !== undefined) ? 
				Math.round((new Date().getTime() - localStorage.timeStamp)/1000)
				: Infinity;
}

let getExpiry = () => {
	return (localStorage.expiresIn !== undefined) ? localStorage.expiresIn : 0;
}

let regInclude = (arr,str) => {
	let regExp;
	for(let url of arr) {
		url = (url.slice(-1) == '*') ? 
				'^' + url.substr(0, url.length-1) + '[0-9A-Za-z]*$' :
				'^' + url + '$';
		regExp = new RegExp(url, "i");
		if(regExp.test(str)) {
			return true;
		}
	}
	return false;
}
