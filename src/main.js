import Vue from 'vue';
import TopNav from './components/top-nav'
import SideNav from './components/side-nav'
import app from './views/app'
import AddressBookingModal from './components/address-book-modal';
import axios from 'axios';
import router from './router';
import Toasted from 'vue-toasted';
import auth from './middlewares/auth';
import VueSelect from 'vue-cool-select'
import VueRangedatePicker from 'vue-rangedate-picker'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import 'vue-tel-input/dist/vue-tel-input.css'
import VueTelInput from 'vue-tel-input'
import momentjs from 'moment'


Vue.prototype.moment = momentjs;
// import moment from 'moment-timezone'

Vue.use(VueRangedatePicker);
Vue.use(Toasted);
Vue.use(VueTelInput);
 
Vue.use(VueMoment, {
    moment,
})

let token = localStorage.token ? localStorage.token: ''


axios.interceptors.request.use((config) => {
	config.headers.Authorization = `Bearer ${localStorage.token}`;
	config.baseURL = API_URL;

	 return config;
});

// // before a request is made start the nprogress
// axios.interceptors.request.use(config => {
//   NProgress.start()
//   return config
// })

// // before a response is returned stop nprogress
// axios.interceptors.response.use(response => {
//   NProgress.done()
//   return response
// })


Vue.prototype.axios = axios;

window.baseUrl = API_URL;

Vue.component('app',app);
Vue.component('top-nav',TopNav);
Vue.component('side-nav',SideNav);
Vue.component('address-book-modal', AddressBookingModal);



new Vue({
	el: '#app',
	router
});


